# classy Diagram Viewer
## About
classy helps you generate [PlantUML](https://plantuml.com) class diagrams for LabVIEW code. By using classy you become part of the [Docs as Code](https://www.writethedocs.org/guide/docs-as-code/) movement.

## Features
### Analysis
- Relations between classes: inheritance, composition, aggregation
- Methods: name, scope, static/dynamic type indication
- Fields: name, scope, datatype (to certain extent)
- Packages: grouping by `lvlib`

### API
classy's API can be used within larger documentation generation projects, e.g. [Antidoc](https://gitlab.com/wovalab/open-source/labview-doc-generator). At the moment the following functions are provided:

- `Class To String`: class reference -> PlantUML text
- `Target To String`: target reference -> PlantUML text
- `Project To String`: project path -> PlantUML text per target

### UI / Project Provider
🚧 How nice would it be to right click anywhere in the project and see a diagram for that specific component--or multiple components! Unfortunately there are some PP related challenges that seem to be difficult to overcome. Project Provider ninjas are more than welcome to сonvince us otherwise. 🚧

### Actor Framework Projects Support
🚧 We realize that AF based projects need to be treated in a special way. Please hold on or consider becoming a contributor! 🚧

## Compatibility
Main development is done in LV 2020 CE.
Antidoc related branches hold code downgraded to LV 2014.
