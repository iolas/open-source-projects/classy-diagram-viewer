# Ways to contribute
Thanks for considering to become a contributor! Here's how you can help:

- Report a bug 🐞
- Submit a fix 🕵️
- Propose/implement new features 🚀
- Become a maintainer 😎

# Language
Development is done in LabVIEW 2020 CE but all API related code shall be ready to be downgraded to LabVIEW 2014 SP1. This is due to the [Antidoc](https://gitlab.com/wovalab/open-source/labview-doc-generator) project that depends on classy. This might change in the future.

At the moment this project aims to use LabVIEW as much as possible (read: 🚫🐍).

# Repository / issue tracking
GitLab is used to host code, to track issues and feature requests, as well as accept merge requests. All code changes happen through merge requests. That said, please start with proposing a new feature rather than creating a merge request immediately.

# Commit messages
Don't write [bad commit messages](http://whatthecommit.com/), do write [good commit messages](https://chris.beams.io/posts/git-commit/). Some people are [crazy about it](https://tatiana.boye.gitlab.io/a-furious-monologue-on-commit-messages/#/).

# Reporting a bug
Report a bug by [opening a new issue](https://gitlab.com/tatiana.boye/classy-diagram-viewer/-/issues/new). Please write bug reports with detail, background, and steps to reproduce / sample code. Describe what you expected would happen and what actually happens. Thourough bug reports are cool!

# Proposing new features 
Propose new feature by [opening a new issue](https://gitlab.com/tatiana.boye/classy-diagram-viewer/-/issues/new). Please indicate whether you'd like to work on it yourself. Either way, we'll get back to you as soon as possible.

# Submitting changes
[Fork the project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html), implement your changes and [create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork).

# Becoming a maintainer
Just drop a line at [tatiana.boye@me.com](mailto:tatiana.boye@me.com).

# License
By contributing, you agree that your contributions will be licensed under the current BSD 3-Clause License.
